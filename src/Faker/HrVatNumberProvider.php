<?php

namespace Superius\VatNumberRuler\Faker;

use Faker\Provider\Base;
use Superius\VatNumberRuler\Services\Checksum\ChecksumIso7064Mod1110;

class HrVatNumberProvider extends Base
{
    /**
     * Generate a si vat number
     *
     * @see Superius\VatNumberRuler\Rules\SiVatNumber
     * @return string
     */
    public function hrVatNumber(): string
    {
        $oibNumberBase = self::numerify(str_repeat('#', 10));

        $checkDigit = ChecksumIso7064Mod1110::calculateCheckDigit($oibNumberBase);
        $vatNumber = $oibNumberBase . $checkDigit;

        if (ChecksumIso7064Mod1110::isHrVatNumberValid($vatNumber)) {
            return $vatNumber;
        }

        throw new \RuntimeException('Failed to generate a valid hr vat number');
    }
}
