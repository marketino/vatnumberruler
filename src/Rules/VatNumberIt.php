<?php

namespace Superius\VatNumberRuler\Rules;

use Illuminate\Contracts\Validation\Rule;

class VatNumberIt implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @throws \Safe\Exceptions\PcreException
     */
    public function passes($attribute, $value):bool
    {
        return self::isValidVATNumberIt($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message():string
    {
        return __('The vat number must be valid.');
    }

    /**
     * http://www.icosaedro.it/cf-pi/pi-php.txt
     * @param string $vatNumber
     * @return bool
     */
    public static function isValidVATNumberIt(string $vatNumber): bool
    {

        if (!$vatNumber || strlen($vatNumber) !== 11) {
            return false;
        }

        if (preg_match('/^\d{11}$/D', $vatNumber) !== 1) {
            return false;
        }

        $s = 0;
        for ($i = 0; $i <= 9; $i += 2) {
            $s += ord($vatNumber[$i]) - ord('0');
        }

        for ($i = 1; $i <= 9; $i += 2) {
            $c = 2 * (ord($vatNumber[$i]) - ord('0'));
            if ($c > 9) {
                $c -= 9;
            }
            $s += $c;
        }

        return (10 - $s % 10) % 10 === ord($vatNumber[10]) - ord('0');
    }
}
