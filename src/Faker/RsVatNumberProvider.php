<?php

namespace Superius\VatNumberRuler\Faker;

use Faker\Provider\Base;
use Superius\VatNumberRuler\Services\Checksum\ChecksumIso7064Mod1110;

class RsVatNumberProvider extends Base
{
    /**
     * Generate a rs vat number
     *
     * @see Superius\VatNumberRuler\Rules\RsVatNumber
     * @return string
     */
    public function rsVatNumber(): string
    {
        $oibNumberBase = self::numberBetween(10000001, 99999999);
        $checkDigit = ChecksumIso7064Mod1110::calculateCheckDigit($oibNumberBase);
        $vatNumber = $oibNumberBase . $checkDigit;

        if (ChecksumIso7064Mod1110::isRsVatNumberValid($vatNumber)) {
            return $vatNumber;
        }

        throw new \RuntimeException('Failed to generate a valid rs vat number');
    }
}
