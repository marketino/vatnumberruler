<?php

namespace Superius\VatNumberRuler\Faker;

use Faker\Provider\Base;
use Superius\VatNumberRuler\Services\Checksum\ChecksumMod11;

class SiVatNumberProvider extends Base
{
    /**
     * Generate a si vat number
     *
     * @see Superius\VatNumberRuler\Rules\SiVatNumber
     * @return string
     */
    public function siVatNumber(): string
    {
        $vatNumberBase = self::numberBetween(1000001,9999998);

        $checkDigit = ChecksumMod11::calculateCheckDigit($vatNumberBase);
        $vatNumber = $vatNumberBase . $checkDigit;
        if (ChecksumMod11::isValid($vatNumber)) {
            return $vatNumber;
        }

        // failed to generate vat in first try
        $lastDigit = $vatNumberBase % 10;
        $vatNumberBase += -$lastDigit + (($lastDigit + 1) % 10);
        $checkDigit = ChecksumMod11::calculateCheckDigit($vatNumberBase);
        $vatNumber = $vatNumberBase . $checkDigit;
        if (ChecksumMod11::isValid($vatNumber)) {
            return $vatNumber;
        }

        throw new \RuntimeException('failed to generate a valid vat number');
    }
}
