<?php

namespace Superius\VatNumberRuler\Rules;

use Illuminate\Contracts\Validation\Rule;

class VatNumberSi implements Rule
{
    public function __construct(private readonly bool $mustHaveSiPrefix = false)
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (!$this->mustHaveSiPrefix) {
            return self::isValidVATNumberSi($value);
        }

        $prefix = substr($value, 0, 2);
        $value = substr($value, 2);

        return strtolower($prefix) === 'si' && self::isValidVATNumberSi($value);
    }

    /**
     * Provjeri ispravnost OIB-a.
     *
     * @param string $oib Vrijednost OIB-a
     * @return boolean True ako je ispravan, inače false.
     *
     */
    public static function isValidVATNumberSi(string $oib): bool
    {
        if (!preg_match('/^\d{8}$/', $oib)) {
            return false;
        }

        $total = 0;
        $multipliers = [8, 7, 6, 5, 4, 3, 2];

        // Extract the next digit and multiply by the counter.
        for ($i = 0; $i < 7; $i++) {
            $total += (int)$oib[$i] * $multipliers[$i];
        }

        // Establish check digits using modulus 11
        $total = 11 - $total % 11;
        if ($total == 10) {
            $total = 0;
        }

        if ($total != 11 && $total == $oib[7]) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('The vat number must be valid.');
    }
}
