<?php

namespace Superius\VatNumberRuler\Providers;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\ServiceProvider;
use Superius\VatNumberRuler\Faker\HrVatNumberProvider;
use Superius\VatNumberRuler\Faker\RsVatNumberProvider;
use Superius\VatNumberRuler\Faker\SiVatNumberProvider;
use Superius\VatNumberRuler\Faker\UsVatNumberProvider;

class VatNumberServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->extend(Generator::class, function ($faker) {
            $faker->addProvider(new HrVatNumberProvider($faker));
            $faker->addProvider(new RsVatNumberProvider($faker));
            $faker->addProvider(new SiVatNumberProvider($faker));
            $faker->addProvider(new UsVatNumberProvider($faker));

            return $faker;
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {

    }
}
